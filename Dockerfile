FROM ubuntu
RUN apt-get update && apt-get install -y \
	git \
	apache2  
	
RUN git clone https://github.com/letsencrypt/letsencrypt

COPY ./run.sh ./

RUN ./letsencrypt/letsencrypt-auto -h \
	--no-install-recommends \
	&& rm -rf /var/lib/apt/lists/* \
	;rm -Rf /var/cache \
	;rm /tmp/* \
	;apt-get autoremove -y \
	;chmod +x ./run.sh
	

EXPOSE 80 443

CMD  ./run.sh